---
title: "Tidy Tuesday - Week 13"
subtitle: "Alcohol Consumption"
output: html_notebook
---

```{r}
library(tidyverse)
library(plotly)
library(wesanderson)
```

```{r}
my_theme <- theme(text = element_text(family = "Comfortaa", color = "#22211d"),
                  plot.background = element_rect(fill = "#f5f5f2", color = NA), 
                  panel.background = element_rect(fill = "#f5f5f2", color = NA), 
                  legend.background = element_rect(fill = "#f5f5f2", color = NA),
                  legend.key = element_rect(fill = "#f5f5f2"),
                  plot.caption = element_text(size = 6))
```


```{r}

df <- read_csv("week13_alcohol_global.csv")
df
```

```{r}
# list of regions and countries
# https://www.thoughtco.com/official-listing-of-countries-world-region-1435153

regions <- read_csv("regions.csv")
head(regions)

regions <- arrange(regions)

dim(df)

matches <- df$country %in% regions$Country

sum(matches)

which(!matches)


```

```{r}
df$country[!matches]
#setdiff(df$country, regions$Country)
```

```{r}

regions_mod <- regions
colnames(regions_mod) <- c("country","region")

regions_mod$country[regions_mod$country == "Bosnia and Herzegovina"] <- "Bosnia-Herzegovina"
regions_mod$country[regions_mod$country == "Antigua and Barbuda"] <- "Antigua & Barbuda"
regions_mod$country[regions_mod$country == "The Bahamas"] <- "Bahamas"
regions_mod$country[regions_mod$country == "Cape Verde"] <- "Cabo Verde"
regions_mod$country[regions_mod$country == "The Central African Republic"] <- "Central African Republic"
regions_mod$country[regions_mod$country == "Republic of the Congo"] <- "Congo"
regions_mod$country[regions_mod$country == "The Democratic Republic of the Congo"] <- "DR Congo"
regions_mod <- rbind(regions_mod, c("Cook Islands", "Australia and Oceania"))
regions_mod$country[regions_mod$country == "The Gambia"] <- "Gambia"
regions_mod$country[regions_mod$country == "The Federated States of Micronesia"] <- "Micronesia"
regions_mod <- rbind(regions_mod, c("Niue", "Australia and Oceania"))
regions_mod$country[regions_mod$country == "Russia"] <- "Russian Federation"
regions_mod$country[regions_mod$country == "Saint Kitts and Nevis"] <- "St. Kitts & Nevis"
regions_mod$country[regions_mod$country == "Saint Lucia"] <- "St. Lucia"
regions_mod$country[regions_mod$country == "Saint Vincent and the Grenadines"] <- "St. Vincent & the Grenadines"
regions_mod$country[regions_mod$country == "Sao Tome and Principe"] <- "Sao Tome & Principe"
regions_mod$country[regions_mod$country == "East Timor"] <- "Timor-Leste"
regions_mod$country[regions_mod$country == "Trinidad and Tobago"] <- "Trinidad & Tobago"
regions_mod$country[regions_mod$country == "The United Arab Emirates"] <- "United Arab Emirates"
regions_mod$country[regions_mod$country == "United Kingdom of Great Britain and Northern Ireland"] <- "United Kingdom"
regions_mod$country[regions_mod$country == "The United States of America"] <- "USA"

regions_mod$country[97] <- "United Kingdom"

setdiff(df$country, regions_mod$country)

```

```{r}

df2 <- left_join(df, regions_mod, by = "country")

```



```{r fig.width=9}

p <- plot_ly(df2, x = ~beer_servings, y = ~spirit_servings, z = ~wine_servings, color = ~region, 
             mode = 'text', text = ~country, marker = list(size = 4)) %>%
  #add_markers() %>%
  layout(scene = list(xaxis = list(title = 'Beer'),
                      yaxis = list(title = 'Spirits'),
                      zaxis = list(title = 'Wine')))

#chart_link = api_create(p, filename="TidyTuesdayWeek13")
#chart_link
p
```

```{r fig.height=8, fig.width=9}


p2 <- df2 %>%
  gather(key = "beverage", value = "value", 2:4) %>%
  ggplot() +
  geom_jitter(aes(x = region, y = value, color = beverage), alpha = 0.6, size = 3, width = 0.3) +
  scale_color_manual(values = wes_palettes$FantasticFox1, labels = c("Beer","Spirits","Wine"), name = "Beverage", 
                     guide = guide_legend(direction = 'horizontal', title.position = 'top', label.position = 'right',
                                          keywidth = unit(3, "mm"))) +
  scale_x_discrete(labels = c("Asia","Australia\n& Oceania", "Central America /\nCaribbean", "Europe", "Middle East", "N America", "S America", "Africa")) +
  labs(x="",y="No of Servings",
       title = "Alcohol Consumption by Region",
       caption = "By @DaveBloom11\t\t\tSources: FiveThirtyEight, ThoughtCo.") +
  theme_minimal() + my_theme +
  theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1),
        legend.position = c(0.85,0.93),
        legend.background = element_blank(),
        legend.key = element_blank())

p2
#ggplotly(p2)
ggsave("bev.png")

```

